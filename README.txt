votingapi_service is a Drupal module that provides access to Voting API
methods through Services. This allows external web applications to access and
modify voting data for Drupal objects.

votingapi_service provides the following methods:

- setVote: Set vote for specified content from a given user.
- unsetVote: Remove vote for specified content from a given user.
- getUserVotes: Get all votes for specified content from a given user.
- getContentVotes: Get all votes for specified content.
- getVotingResults: Get results (current average) for specified content.

votingapi_service allows only users with permissions of 'access votes' or of
'edit votes' to use these methods.
